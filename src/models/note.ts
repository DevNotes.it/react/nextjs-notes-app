export class Note{
    id: number;
    public title: String = "";
    public content: String = "";
    public tags: String[];
}