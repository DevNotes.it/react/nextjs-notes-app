import React from 'react';
import {ThemeProvider} from "@material-ui/styles";
import {Container, createMuiTheme, CssBaseline, Paper} from "@material-ui/core";
import AppBar from "./app-bar";

const theme = createMuiTheme({
    palette: {
        type: "dark"
    }
});

const Template = ({children}) => {
    return (
        <ThemeProvider theme={theme}>
            <CssBaseline/>
            <AppBar/>
            <Container style={{marginTop:"100px"}}>
                {children}
            </Container>
        </ThemeProvider>
    );
};

export default Template;